import java.util.Scanner;

public class Solution {
    public int[] rowAndMaximumOnes(int[][] mat) {
        int maxOnesRowIndex = 0;
        int maxOnesCount = 0;

        for (int i = 0; i < mat.length; i++) {
            int onesCount = countOnes(mat[i]);
            if (onesCount > maxOnesCount) {
                maxOnesCount = onesCount;
                maxOnesRowIndex = i;
            }
        }

        return new int[]{maxOnesRowIndex, maxOnesCount};
    }

    private int countOnes(int[] row) {
        int count = 0;
        for (int num : row) {
            if (num == 1) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int m = scanner.nextInt();

        System.out.print("Enter the number of columns: ");
        int n = scanner.nextInt();

        int[][] mat = new int[m][n];

        System.out.println("Enter the elements of the matrix (0 or 1):");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                mat[i][j] = scanner.nextInt();
            }
        }

        Solution solution = new Solution();
        int[] result = solution.rowAndMaximumOnes(mat);

        System.out.println("Row index with the maximum count of ones: " + result[0]);
        System.out.println("Number of ones in that row: " + result[1]);
    }
}
